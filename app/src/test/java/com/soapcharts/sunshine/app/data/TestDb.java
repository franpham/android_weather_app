package com.soapcharts.sunshine.app.data;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.test.AndroidTestCase;

import com.soapcharts.sunshine.app.data.WeatherContract.*;
import java.util.HashSet;

public class TestDb extends AndroidTestCase {
	// AndroidTestCase provides a reference to the Context called, mContext

	public static final String TESTDB_LOGTAG = TestDb.class.getSimpleName();

	// Since we want each test to start with a clean slate
	void deleteTheDatabase() {
		mContext.deleteDatabase(WeatherDbHelper.DATABASE_NAME);
	}

	// This function gets called before each test is executed to delete the database.  This makes sure that we always have a clean test.
	public void setUp() {
		deleteTheDatabase();
	}

	public void testCreateDb() throws Throwable {		// tests if the location table was created correctly;
		// build a HashSet of all of the table names we wish to look for
		// Note that there will be another table in the DB that stores the
		// Android metadata (db version information)
		final HashSet<String> tableNameHashSet = new HashSet<String>();
		tableNameHashSet.add(WeatherContract.LocationEntry.TABLE_NAME);
		tableNameHashSet.add(WeatherContract.WeatherEntry.TABLE_NAME);

		mContext.deleteDatabase(WeatherDbHelper.DATABASE_NAME);
		SQLiteDatabase db = new WeatherDbHelper(
				this.mContext).getWritableDatabase();
		assertEquals(true, db.isOpen());

		// have we created the tables we want?
		Cursor cursor = db.rawQuery("SELECT name FROM sqlite_master WHERE type='table'", null);
		assertTrue("Error: This means that the database has not been created correctly",
				cursor.moveToFirst());

		// verify that the tables have been created
		do {
			tableNameHashSet.remove(cursor.getString(0));
		} while( cursor.moveToNext() );
		// if this fails, it means that your database doesn't contain both the location entry and weather entry tables
		assertTrue("Error: Your database was created without both the location entry and weather entry tables",
				tableNameHashSet.isEmpty());

		// now, do our tables contain the correct columns?
		cursor = db.rawQuery("PRAGMA table_info(" + WeatherContract.LocationEntry.TABLE_NAME + ")",
				null);
		assertTrue("Error: This means that we were unable to query the database for table information.",
				cursor.moveToFirst());

		// Build a HashSet of all of the column names we want to look for
		final HashSet<String> locationColumnHashSet = new HashSet<String>();
		locationColumnHashSet.add(WeatherContract.LocationEntry._ID);
		locationColumnHashSet.add(WeatherContract.LocationEntry.COLUMN_CITY_NAME);
		locationColumnHashSet.add(WeatherContract.LocationEntry.COLUMN_COORD_LAT);
		locationColumnHashSet.add(WeatherContract.LocationEntry.COLUMN_COORD_LONG);
		locationColumnHashSet.add(WeatherContract.LocationEntry.COLUMN_LOCATION_SETTING);

		int columnNameIndex = cursor.getColumnIndex("name");
		do {
			String columnName = cursor.getString(columnNameIndex);
			locationColumnHashSet.remove(columnName);
		} while(cursor.moveToNext());

		// if this fails, it means that your database doesn't contain all of the required location entry columns
		assertTrue("Error: The database doesn't contain all of the required location entry columns",
				locationColumnHashSet.isEmpty());
		cursor.close();
	}

	/*
		Students:  Here is where you will build code to test that we can insert and query the
		location database.  We've done a lot of work for you.  You'll want to look in TestUtilities
		where you can uncomment out the "createLocationValues" function.  You can
		also make use of the ValidateCurrentRecord function from within TestUtilities.  Return
		the rowId of the inserted location.
	*/
	public long testLocationTable() {
		// First step: Get reference to writable database
		SQLiteDatabase db = new WeatherDbHelper(this.mContext).getWritableDatabase();
		assertTrue(db.isOpen());

		ContentValues values = TestUtilities.createLocationValues();
		long rowId = TestUtilities.insertLocationValues(this.mContext, values);
		// Create ContentValues of what you want to insert
		// Insert ContentValues into database and get a row ID back

		// Query the database and receive a Cursor back;
		// query(table_name, [columns array], WHERE query, [query arguments array], GROUP BY string, HAVING string, ORDER BY string, LIMIT string)
		Cursor cursor = db.query(LocationEntry.TABLE_NAME, null,
				LocationEntry.COLUMN_LOCATION_SETTING + " = ? ", new String [] {TestUtilities.TEST_LOCATION}, null, null, LocationEntry.COLUMN_CITY_NAME + " ASC ");

		TestUtilities.validateCurrentRecord(TESTDB_LOGTAG, cursor, values);
		assertFalse("More than one record returned for location query", cursor.moveToNext());
		cursor.close();						// Validate data in resulting Cursor with the original ContentValues
		db.close();
		return rowId;						// == -1 if not inserted
	}


	/*
		Students:  Here is where you will build code to test that we can insert and query the
		database.  We've done a lot of work for you.  You'll want to look in TestUtilities
		where you can use the "createWeatherValues" function.  You can
		also make use of the validateCurrentRecord function from within TestUtilities.
	 */
	public long testWeatherTable() {		// NOTE: modified to return the inserted rowId of weather table;
		long locationId = testLocationTable();
		assertFalse("Location not inserted correctly", locationId == -1L);

		// First insert the location, and then use the locationRowId to insert the weather.
		// First step: Get reference to writable database
		SQLiteDatabase db = new WeatherDbHelper(this.mContext).getWritableDatabase();
		assertTrue(db.isOpen());

		ContentValues values = TestUtilities.createWeatherValues(locationId);
		long weatherId = TestUtilities.insertWeatherValues(this.mContext, values);
		// Create ContentValues of what you want to insert
		// Insert ContentValues into database and get a row ID back

		// Query the database and receive a Cursor back
		Cursor cursor = db.query(WeatherEntry.TABLE_NAME, null,
				WeatherEntry.COLUMN_DATE + " = ? ", new String[] {Long.toString(TestUtilities.TEST_DATE)}, null, null, WeatherEntry.COLUMN_DATE + " DESC ");

		TestUtilities.validateCurrentRecord(TESTDB_LOGTAG, cursor, values);
		assertFalse("More than one record returned for weather query", cursor.moveToNext());
		cursor.close();						// Validate data in resulting Cursor with the original ContentValues
		db.close();
		return weatherId;					// == -1 if not inserted;
	}
}
