package com.soapcharts.sunshine.app;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;

// This Activity and its WeatherFragment are created only in phone mode, as determined by MainActivity.onDateSelected;
public class DetailActivity extends ActionBarActivity {
	// this Activity does not have Settings menu because it does not monitor location changes;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_detail);
		if (savedInstanceState == null) {
			Bundle args = new Bundle();
			args.putParcelable(WeatherFragment.DETAIL_URI, getIntent().getData());		// pass the dateUri;
			WeatherFragment fragment = new WeatherFragment();
			fragment.setArguments(args);

			getSupportFragmentManager().beginTransaction()		// dynamically add WeatherFragment's layout to detail_container;
					.add(R.id.detail_container, fragment).commit();
		}
	}
}
