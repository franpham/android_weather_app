package com.soapcharts.sunshine.app;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.text.format.Time;

import com.soapcharts.sunshine.app.data.WeatherContract;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Utility {
	public static final int FORECAST_LOADER = 1;
	// MUST BE unique in app; Loaders are registered, and not linked to Activity;

	public static final String[] FORECAST_COLUMNS = {
			WeatherContract.WeatherEntry.TABLE_NAME + "." + WeatherContract.WeatherEntry._ID,
			WeatherContract.WeatherEntry.COLUMN_DATE,
			WeatherContract.WeatherEntry.COLUMN_SHORT_DESC,
			WeatherContract.WeatherEntry.COLUMN_MAX_TEMP,
			WeatherContract.WeatherEntry.COLUMN_MIN_TEMP,
			WeatherContract.WeatherEntry.COLUMN_CONDITION_ID,
			WeatherContract.LocationEntry.COLUMN_LOCATION_SETTING,
			WeatherContract.LocationEntry.COLUMN_CITY_NAME,
			WeatherContract.WeatherEntry.COLUMN_DEGREES,
			WeatherContract.WeatherEntry.COLUMN_WIND_SPEED,
			WeatherContract.WeatherEntry.COLUMN_HUMIDITY,
			WeatherContract.WeatherEntry.COLUMN_PRESSURE,
			WeatherContract.LocationEntry.COLUMN_COORD_LAT,
			WeatherContract.LocationEntry.COLUMN_COORD_LONG
	};

	// Since the database returns data in the order specified by the projection parameter, if FORECAST_COLUMNS changes these constants must change.
	public static final int COL_WEATHER_ID = 0;
	public static final int COL_WEATHER_DATE = 1;
	public static final int COL_WEATHER_DESC = 2;
	public static final int COL_WEATHER_MAX_TEMP = 3;
	public static final int COL_WEATHER_MIN_TEMP = 4;
	public static final int COL_CONDITION_ID = 5;
	public static final int COL_LOCATION_SETTING = 6;
	public static final int COL_CITY_NAME = 7;
	public static final int COL_WIND_DEGREES = 8;
	public static final int COL_WIND_SPEED = 9;
	public static final int COL_HUMIDITY = 10;
	public static final int COL_PRESSURE = 11;
	public static final int COL_COORD_LAT = 12;
	public static final int COL_COORD_LONG= 13;

	public static boolean isNoticeEnabled(Context context) {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
		return prefs.getBoolean(context.getString(R.string.pref_notice_key), true);
	}

	public static String getPreferredLocation(Context context) {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
		return prefs.getString(context.getString(R.string.pref_location_key), context.getString(R.string.pref_location_default));
	}

	public static boolean isMetric(Context context) {
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
		String value = prefs.getString(context.getString(R.string.pref_temp_key), context.getString(R.string.pref_temp_imperial));
		return value.equals(context.getString(R.string.pref_temp_metric));
	}

	public static Uri getGeoLocation(Context context) {
		String zipCode = getPreferredLocation(context);
		return Uri.parse("geo:0,0?").buildUpon().appendQueryParameter("q", zipCode).build();
	}

	public static Uri getGeoLocation(Cursor cursor) {
		if (cursor.moveToFirst()) {
			String latitude = cursor.getString(COL_COORD_LAT);
			String longitude= cursor.getString(COL_COORD_LONG);
			return Uri.parse("geo:" + latitude + "," + longitude);
		}
		return null;
	}

	public static String convertCursorRowToUXFormat(Context context, Cursor cursor) {
		Date date = new Date(cursor.getLong(COL_WEATHER_DATE));
		String formattedDate = DateFormat.getDateInstance().format(date);
		String highLow = formatTemperature(context, cursor.getDouble(COL_WEATHER_MAX_TEMP))
				+ " / " + formatTemperature(context, cursor.getDouble(COL_WEATHER_MIN_TEMP));
		return formattedDate + " - " + cursor.getString(COL_WEATHER_DESC) + " - " + highLow;
	}

	public static String formatTemperature(Context context, double temperature) {
		boolean isMetric = isMetric(context);
		double temp;
		if ( !isMetric ) {
			temp = 9*temperature/5+32;
		} else {
			temp = temperature;
		}
		return context.getString(R.string.format_temperature, temp);
	}

	public static String getFormattedWind(Context context, float windSpeed, float degrees) {
		int windFormat;
		if (Utility.isMetric(context)) {
			windFormat = R.string.format_wind_kmh;
		} else {
			windFormat = R.string.format_wind_mph;
			windSpeed = .621371192237334f * windSpeed;
		}
		// From wind direction in degrees, determine compass direction as a string (e.g NW)
		String direction = "Unknown";
		if (degrees >= 337.5 || degrees < 22.5) {
			direction = "N";
		} else if (degrees >= 22.5 && degrees < 67.5) {
			direction = "NE";
		} else if (degrees >= 67.5 && degrees < 112.5) {
			direction = "E";
		} else if (degrees >= 112.5 && degrees < 157.5) {
			direction = "SE";
		} else if (degrees >= 157.5 && degrees < 202.5) {
			direction = "S";
		} else if (degrees >= 202.5 && degrees < 247.5) {
			direction = "SW";
		} else if (degrees >= 247.5 && degrees < 292.5) {
			direction = "W";
		} else if (degrees >= 292.5 || degrees < 22.5) {
			direction = "NW";
		}
		return String.format(context.getString(windFormat), windSpeed, direction);
	}

	/**
	 * @param context Context to use for resource localization
	 * @param dateInMillis The date in milliseconds
	 * @return a user-friendly representation of the date.
	 */
	public static String getFormattedDate(Context context, long dateInMillis) {
		// The day string for forecast uses the following logic:
		// For today, tomorrow: "Today, December 8"
		// For the next 6 days: "Tomorrow" or day name;
		// For all days after that: "Mon, December 8"

		Time time = new Time();
		time.setToNow();
		long currentTime = System.currentTimeMillis();
		int julianDay = Time.getJulianDay(dateInMillis, time.gmtoff);
		int currentJulianDay = Time.getJulianDay(currentTime, time.gmtoff);

		if (julianDay == currentJulianDay) {
			String today = context.getString(R.string.today);
			int formatId = R.string.format_friendly_date;
			return context.getString(formatId, today, getMonthDate(dateInMillis));
		} else if ( julianDay < currentJulianDay + 7 ) {
			return getDayName(context, dateInMillis);
		} else {
			return new SimpleDateFormat("EEE', '").format(dateInMillis) + getMonthDate(dateInMillis);
		}
	}

	/**
	 * Given a day, returns just the name to use for that day.
	 * E.g "today", "tomorrow", "wednesday".
	 * @param context Context to use for resource localization
	 * @param dateInMillis The date in milliseconds
	 * @return the day name
	 */
	public static String getDayName(Context context, long dateInMillis) {
		Time t = new Time();
		t.setToNow();
		int julianDay = Time.getJulianDay(dateInMillis, t.gmtoff);
		int currentJulianDay = Time.getJulianDay(System.currentTimeMillis(), t.gmtoff);
		if (julianDay == currentJulianDay) {
			return context.getString(R.string.today);
		} else if ( julianDay == currentJulianDay +1 ) {
			return context.getString(R.string.tomorrow);
		} else {
			SimpleDateFormat dayFormat = new SimpleDateFormat("EEEE");
			return dayFormat.format(dateInMillis);
		}
	}

	/**
	 * Converts db date format to the format "Month day", e.g "June 24".
	 * @return The day in the form of a string formatted "December 6"
	 */
	public static String getMonthDate(long dateInMillis) {
		SimpleDateFormat shortenedDateFormat = new SimpleDateFormat("MMM dd");
		return shortenedDateFormat.format(dateInMillis);
	}

	/**
	 * Helper method to provide the icon resource id according to the weather condition id returned
	 * by the OpenWeatherMap call.
	 * @param weatherId from OpenWeatherMap API response
	 * @return resource id for the corresponding icon. -1 if no relation is found.
	 */
	public static int getIconForWeatherId(int weatherId) {
		// Based on weather code data found at:
		// http://bugs.openweathermap.org/projects/api/wiki/Weather_Condition_Codes
		if (weatherId >= 200 && weatherId <= 232) {
			return R.drawable.ic_storm;
		} else if (weatherId >= 300 && weatherId <= 321) {
			return R.drawable.ic_light_rain;
		} else if (weatherId >= 500 && weatherId <= 504) {
			return R.drawable.ic_rain;
		} else if (weatherId == 511) {
			return R.drawable.ic_snow;
		} else if (weatherId >= 520 && weatherId <= 531) {
			return R.drawable.ic_rain;
		} else if (weatherId >= 600 && weatherId <= 622) {
			return R.drawable.ic_snow;
		} else if (weatherId >= 701 && weatherId <= 761) {
			return R.drawable.ic_fog;
		} else if (weatherId == 761 || weatherId == 781) {
			return R.drawable.ic_storm;
		} else if (weatherId == 800) {
			return R.drawable.ic_clear;
		} else if (weatherId == 801) {
			return R.drawable.ic_light_clouds;
		} else if (weatherId >= 802 && weatherId <= 804) {
			return R.drawable.ic_cloudy;
		}
		return -1;
	}

	/**
	 * Helper method to provide the art resource id according to the weather condition id returned
	 * by the OpenWeatherMap call.
	 * @param weatherId from OpenWeatherMap API response
	 * @return resource id for the corresponding image. -1 if no relation is found.
	 */
	public static int getArtForWeatherId(int weatherId) {
		// Based on weather code data found at:
		// http://bugs.openweathermap.org/projects/api/wiki/Weather_Condition_Codes
		if (weatherId >= 200 && weatherId <= 232) {
			return R.drawable.art_storm;
		} else if (weatherId >= 300 && weatherId <= 321) {
			return R.drawable.art_light_rain;
		} else if (weatherId >= 500 && weatherId <= 504) {
			return R.drawable.art_rain;
		} else if (weatherId == 511) {
			return R.drawable.art_snow;
		} else if (weatherId >= 520 && weatherId <= 531) {
			return R.drawable.art_rain;
		} else if (weatherId >= 600 && weatherId <= 622) {
			return R.drawable.art_snow;
		} else if (weatherId >= 701 && weatherId <= 761) {
			return R.drawable.art_fog;
		} else if (weatherId == 761 || weatherId == 781) {
			return R.drawable.art_storm;
		} else if (weatherId == 800) {
			return R.drawable.art_clear;
		} else if (weatherId == 801) {
			return R.drawable.art_light_clouds;
		} else if (weatherId >= 802 && weatherId <= 804) {
			return R.drawable.art_clouds;
		}
		return -1;
	}
}