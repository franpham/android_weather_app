/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.soapcharts.sunshine.app.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.soapcharts.sunshine.app.data.WeatherContract.LocationEntry;
import com.soapcharts.sunshine.app.data.WeatherContract.WeatherEntry;

/**
 * Manages a local database for weather data.
 */
public class WeatherDbHelper extends SQLiteOpenHelper {
	private static final String LOG_TAG = WeatherDbHelper.class.getSimpleName();

    // If you change the database schema, you must increment the database version.
    private static final int DATABASE_VERSION = 2;
    static final String DATABASE_NAME = "weather.db";

    public WeatherDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override		// if table already exists, onOpen method is called instead;
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
		// For weather forecasting, it's reasonable to assume the user will want information
		// for a certain date and all dates *following*, so the forecast data should be sorted accordingly.
        final String SQL_CREATE_WEATHER_TABLE = "CREATE TABLE " + WeatherEntry.TABLE_NAME + " (" +
                WeatherEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                WeatherEntry.COLUMN_LOC_KEY + " INTEGER NOT NULL, " +
                WeatherEntry.COLUMN_DATE + " INTEGER NOT NULL, " +
                WeatherEntry.COLUMN_SHORT_DESC + " TEXT NOT NULL, " +
                WeatherEntry.COLUMN_CONDITION_ID + " INTEGER NOT NULL, " +

                WeatherEntry.COLUMN_MIN_TEMP + " REAL NOT NULL, " +
                WeatherEntry.COLUMN_MAX_TEMP + " REAL NOT NULL, " +
                WeatherEntry.COLUMN_HUMIDITY + " REAL NOT NULL, " +
                WeatherEntry.COLUMN_PRESSURE + " REAL NOT NULL, " +
                WeatherEntry.COLUMN_WIND_SPEED + " REAL NOT NULL, " +
                WeatherEntry.COLUMN_DEGREES + " REAL NOT NULL, " +

                // Set up the location column as a foreign key to location table.
                " FOREIGN KEY (" + WeatherEntry.COLUMN_LOC_KEY + ") REFERENCES " +
                LocationEntry.TABLE_NAME + " (" + LocationEntry._ID + "), " +
                " UNIQUE (" + WeatherEntry.COLUMN_DATE + " , " +
                WeatherEntry.COLUMN_LOC_KEY + ") ON CONFLICT REPLACE);";

				// To assure the application have just one weather entry per day
				// per location, it's created a UNIQUE constraint with REPLACE strategy
		sqLiteDatabase.execSQL(SQL_CREATE_WEATHER_TABLE);
		Log.d(LOG_TAG, SQL_CREATE_WEATHER_TABLE);

		final String SQL_CREATE_LOCATION_TABLE = "CREATE TABLE " + LocationEntry.TABLE_NAME + " (" +
				LocationEntry._ID + " INTEGER PRIMARY KEY, " +
				LocationEntry.COLUMN_LOCATION_SETTING + " TEXT UNIQUE NOT NULL, " +		// wrap internal strings with spaces;
				LocationEntry.COLUMN_CITY_NAME + " TEXT NOT NULL, " +
				LocationEntry.COLUMN_COORD_LAT + " REAL NOT NULL, " +
				LocationEntry.COLUMN_COORD_LONG + " REAL NOT NULL);";
		sqLiteDatabase.execSQL(SQL_CREATE_LOCATION_TABLE);
		Log.d(LOG_TAG, SQL_CREATE_LOCATION_TABLE);

	}

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        // This database is only a cache for online data, so its upgrade policy is to simply to discard the data and start over.
        // Note that this only fires if you change the version number for your database.
        // It does NOT depend on the version number for your APPLICATION.
        // If you want to update the schema without wiping data, comment out the next 3 lines
        // and call execSQL with an "ALTER TABLE".
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + LocationEntry.TABLE_NAME);
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + WeatherEntry.TABLE_NAME);
        onCreate(sqLiteDatabase);
    }
}
