package com.soapcharts.sunshine.app.sync;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SyncRequest;
import android.content.SyncResult;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.text.format.Time;
import android.util.Log;

import com.soapcharts.sunshine.app.MainActivity;
import com.soapcharts.sunshine.app.R;
import com.soapcharts.sunshine.app.Utility;
import com.soapcharts.sunshine.app.data.WeatherContract.LocationEntry;
import com.soapcharts.sunshine.app.data.WeatherContract.WeatherEntry;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Vector;
/*
To use SyncAdapter: http://developer.android.com/training/sync-adapters/creating-sync-adapter.html
1. copy the 4 classes in the sync package folder AS-IS to create the SyncAdapter and Authenticator;
2. Copy the 2 files in the xml package folder AS-IS to declare the attributes for SyncAdapter and Authenticator;
   define content_authority and sync_account_type for SyncAdapter and Authenticator.
3. Implement getSyncAccount(): if no account, add a new account, set account to sync automatically, and do an initial sync.
4. Implement onPerformSync if do the background work. Code below only does 1-way sync, but 2-way sync is possible.
5. add syncable="true" to the ContentProvider to link the provider with the SyncService.
6. declare the SyncService and AuthenticatorService in AndroidManifest.xml
7. declare the uses-permission for READ_SYNC_SETTINGS, WRITE_SYNC_SETTINGS, AUTHENTICATE_ACCOUNTS.
*/
public class SunshineSyncAdapter extends AbstractThreadedSyncAdapter {
	public static final String LOG_TAG = SunshineSyncAdapter.class.getSimpleName();
	public static final int SYNC_INTERVAL = 60 * 180;		// 60 secs * 180 mins = 3 hours;
	// IMPORTANT: whenever SYNC_INTERVAL is changed, must uninstall the app to test the new time;

	public static final int SYNC_FLEXTIME = SYNC_INTERVAL / 3;
	private static final long DAY_IN_MILLIS = 1000 * 60 * 60 * 24;
	private static final int WEATHER_NOTICE_ID = 1234;

	public SunshineSyncAdapter(Context context, boolean autoInitialize) {
		super(context, autoInitialize);
	}
	// SyncAdapter handles network calls/ errors and syncs data with the ContentProvider;

	public static void initializeSyncAdapter(Context context) {
		PreferenceManager.setDefaultValues(context, R.xml.pref_general, false);
		getSyncAccount(context, true);			// create a new account and syncImmediately;
	}

	/**
	 * Helper method to have the sync adapter sync immediately
	 * @param context The context used to access the account service
	 */
	public static void syncImmediately(Context context) {
		Bundle bundle = new Bundle();
		bundle.putBoolean(ContentResolver.SYNC_EXTRAS_EXPEDITED, true);
		bundle.putBoolean(ContentResolver.SYNC_EXTRAS_MANUAL, true);
		ContentResolver.requestSync(getSyncAccount(context, false),
				context.getString(R.string.content_authority), bundle);
	}

	/**
	 * Helper method to get the fake account to be used with SyncAdapter, or make a new one
	 * if the fake account doesn't exist yet.  If we make a new account, we call the
	 * onAccountCreated method so we can initialize things.
	 *
	 * @param context The context used to access the account service
	 * @return a fake account.
	 */
	public static Account getSyncAccount(Context context, boolean isStarting) {
		AccountManager accountManager = (AccountManager) context.getSystemService(Context.ACCOUNT_SERVICE);
		// Create the default account with sync_account_type
		Account newAccount = new Account(context.getString(R.string.app_name), context.getString(R.string.sync_account_type));

		// If the password doesn't exist, the account doesn't exist OR password was null;
		if (null == accountManager.getPassword(newAccount)) {
			if (!accountManager.addAccountExplicitly(newAccount, "", null))
				return null;		// Add the account with EMPTY password (NOT null) and no user data;
			/*
			 * If you don't set android:syncable="true" in your <provider> element in the manifest,
			 * then call ContentResolver.setIsSyncable(account, AUTHORITY, 1) here.
			 */
			Log.v(LOG_TAG, "In getSyncAccount, password was null so newAccount was added.");
			SunshineSyncAdapter.configurePeriodicSync(newAccount, context.getString(R.string.content_authority));
			ContentResolver.setSyncAutomatically(newAccount, context.getString(R.string.content_authority), true);
			if (isStarting)
				syncImmediately(context);		// IMPORTANT (custom code): added isStarting to prevent recursive calls (should not cause security error);
			// Without calling setSyncAutomatically, our periodic sync will not be enabled. Above 3 lines were in separate onAccountCreated method.
			// IMPORTANT: Syncing needs to be configured only once since Service lifecycle is managed by the system once the account is added.
		}
		return newAccount;
	}

	/**
	 * Helper method to schedule the sync adapter periodic execution
	 */
	public static void configurePeriodicSync(Account account, String authority) {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
			// we can enable inexact timers in our periodic sync
			SyncRequest request = new SyncRequest.Builder().
					syncPeriodic(SYNC_INTERVAL, SYNC_FLEXTIME).
					setSyncAdapter(account, authority).
					setExtras(new Bundle()).build();
			ContentResolver.requestSync(request);
		} else {
			ContentResolver.addPeriodicSync(account, authority, new Bundle(), SYNC_INTERVAL);
		}
	}

	private void notifyWeather() {
		Context context = getContext();
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
		String lastNoticeKey = context.getString(R.string.last_notice_key);
		long lastSync = prefs.getLong(lastNoticeKey, 0);
		long currentTime = System.currentTimeMillis();
		// if (currentTime - lastSync < DAY_IN_MILLIS)		// notify only 1x/ day;
		//	return;
		 if (!Utility.isNoticeEnabled(context))
			return;
		String location = Utility.getPreferredLocation(context);
		Uri dateUri = WeatherEntry.buildWeatherLocationWithDate(location, currentTime);
		Cursor cursor = context.getContentResolver().query(dateUri, Utility.FORECAST_COLUMNS, null, null, null);
		if (cursor.moveToFirst()) {
			int weatherId = cursor.getInt(Utility.COL_CONDITION_ID);
			String high = Utility.formatTemperature(context, cursor.getDouble(Utility.COL_WEATHER_MAX_TEMP));
			String low  = Utility.formatTemperature(context, cursor.getDouble(Utility.COL_WEATHER_MIN_TEMP));
			String desc = cursor.getString(Utility.COL_WEATHER_DESC);
			int iconId = Utility.getIconForWeatherId(weatherId);
			String title = context.getString(R.string.app_name);
			String text = context.getString(R.string.format_notification, desc, high, low);

			SharedPreferences.Editor editor = prefs.edit();
			editor.putLong(lastNoticeKey, currentTime);
			editor.apply();
			NotificationCompat.Builder noticeBuilder = new NotificationCompat.Builder(context)
					.setContentTitle(title)
					.setContentText(text)
					.setSmallIcon(iconId);
			Intent appIntent = new Intent(context, MainActivity.class);		// make a click on Notification open this app;
			// By convention, clicking Back on the Activity should go to Home since app navigation should not be between apps.
			// To implement this, TaskStackBuilder creates an artificial back stack for the started Activity;

			TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
			stackBuilder.addNextIntent(appIntent);
			PendingIntent wrapperIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
			noticeBuilder.setContentIntent(wrapperIntent);
			NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
			manager.notify(WEATHER_NOTICE_ID, noticeBuilder.build());
		}
		cursor.close();
	}

	@Override	// DO NOT create an Intent to WeatherService or FetchWeatherTask since this class is already a service on a separate thread;
	public void onPerformSync(Account account, Bundle extras, String authority, ContentProviderClient provider, SyncResult syncResult) {
		Log.d(LOG_TAG, "Data syncing started.");
		String location = Utility.getPreferredLocation(getContext());

		String format = "json";
		String units = "metric";	// convert units locally so network is not queried again if units are changed;
		// These two need to be declared outside the try/catch so that they can be closed in the finally block.
		HttpURLConnection urlConnection = null;
		BufferedReader reader = null;
		try {
			// Possible parameters are avaiable at OWM's forecast API page, at http://openweathermap.org/API#forecast
			final String FORECAST_BASE_URL = "http://api.openweathermap.org/data/2.5/forecast/daily?";
			final String QUERY_PARAM = "q";
			final String FORMAT_PARAM = "mode";
			final String UNITS_PARAM = "units";
			final String DAYS_PARAM = "cnt";

			// NOTE: website does not recognize all USA zipcodes, ie: 92014 returns an error (view link directly);
			Uri builtUri = Uri.parse(FORECAST_BASE_URL).buildUpon()		// add USA (not US) for country;
					.appendQueryParameter(QUERY_PARAM, location + ",USA")
					.appendQueryParameter(FORMAT_PARAM, format)
					.appendQueryParameter(UNITS_PARAM, units)
					.appendQueryParameter(DAYS_PARAM, "14").build();
			URL url = new URL(builtUri.toString());

			// Create the request to OpenWeatherMap, and open the connection
			urlConnection = (HttpURLConnection) url.openConnection();
			urlConnection.setRequestMethod("GET");
			urlConnection.connect();
			InputStream inputStream = urlConnection.getInputStream();
			if (inputStream == null)
				return;

			String line;
			StringBuffer buffer = new StringBuffer();
			reader = new BufferedReader(new InputStreamReader(inputStream));
			while ((line = reader.readLine()) != null) {
				// Since it's JSON, adding a newline isn't necessary (it won't affect parsing)
				// But it does make debugging a *lot* easier if you print out the completed buffer for debugging.
				buffer.append(line).append("\n");
			}
			if (buffer.length() == 0)
				return;		// Stream was empty. No point in parsing.

			String forecastJsonStr = buffer.toString();
			saveWeatherDataFromJson(forecastJsonStr, location);
		}
		catch (IOException e) {				// cannot show Toast in service thread;
			Log.e(LOG_TAG, e.getMessage(), e);
			Context context = getContext();
			SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(context).edit();
			editor.putString(context.getString(R.string.service_error_key), context.getString(R.string.network_problem));
			editor.commit();
		}
		finally {
			if (urlConnection != null) {
				urlConnection.disconnect();
			}
			if (reader != null) {
				try {
					reader.close();
				} catch (final IOException e) {
					Log.e(LOG_TAG, "Error closing stream. ", e);
				}
			}
		}
	}

	/**
	 * Helper method to handle insertion of a new location in the weather database.
	 * @param locationSetting The location string used to request updates from the server.
	 * @param cityName A human-readable city name, e.g "Mountain View"
	 * @param lat the latitude of the city
	 * @param lon the longitude of the city
	 * @return the row ID of the location.
	 */
	long addLocation(String locationSetting, String cityName, double lat, double lon) {
		long rowId;
		ContentResolver provider = getContext().getContentResolver();
		Cursor cursor = provider.query(LocationEntry.CONTENT_URI, null,
				LocationEntry.COLUMN_LOCATION_SETTING + " = ? ", new String [] {locationSetting}, null);
		if (cursor.moveToFirst())
			rowId = cursor.getLong(cursor.getColumnIndex(LocationEntry._ID));
		else {
			ContentValues values = new ContentValues();
			values.put(LocationEntry.COLUMN_LOCATION_SETTING, locationSetting);
			values.put(LocationEntry.COLUMN_CITY_NAME, cityName);
			values.put(LocationEntry.COLUMN_COORD_LAT, lat);
			values.put(LocationEntry.COLUMN_COORD_LONG, lon);
			// Java 5 automatically converts primitive types to Objects;
			Uri newUri = provider.insert(LocationEntry.CONTENT_URI, values);
			Log.v(LOG_TAG, "Location added: " + cityName);
			rowId = ContentUris.parseId(newUri);
		}
		cursor.close();
		return rowId;
	}

	/**
	 * Take the String representing the complete forecast in JSON Format and
	 * pull out the data we need to construct the Strings needed for the wireframes.
	 */
	private void saveWeatherDataFromJson(String forecastJsonStr, String locationSetting) {
		// These are the names of the JSON objects that need to be extracted.

		final String OWM_CITY = "city";
		final String OWM_CITY_NAME = "name";
		final String OWM_COORD = "coord";
		final String OWM_LATITUDE = "lat";
		final String OWM_LONGITUDE = "lon";
		// Weather information.  Each day's forecast info is an element of the "list" array.
		final String OWM_LIST = "list";
		final String OWM_PRESSURE = "pressure";
		final String OWM_HUMIDITY = "humidity";
		final String OWM_WINDSPEED = "speed";
		final String OWM_WIND_DIRECTION = "deg";
		// All temperatures are children of the "temp" object.
		final String OWM_TEMPERATURE = "temp";
		final String OWM_MAX = "max";
		final String OWM_MIN = "min";
		final String OWM_WEATHER = "weather";
		final String OWM_DESCRIPTION = "main";
		final String OWM_WEATHER_ID = "id";
		try {
			JSONObject forecastJson = new JSONObject(forecastJsonStr);
			JSONArray weatherArray = forecastJson.getJSONArray(OWM_LIST);
			JSONObject cityJson = forecastJson.getJSONObject(OWM_CITY);
			String cityName = cityJson.getString(OWM_CITY_NAME);
			JSONObject cityCoord = cityJson.getJSONObject(OWM_COORD);
			double cityLatitude = cityCoord.getDouble(OWM_LATITUDE);
			double cityLongitude = cityCoord.getDouble(OWM_LONGITUDE);

			long locationId = addLocation(locationSetting, cityName, cityLatitude, cityLongitude);
			// IMPORTANT: add the location to DB (if it doesn't exist) to get location._id for weather table;

			// OWM returns daily forecasts based upon the local time of the city that is being
			// asked for, which means that we need to know the GMT offset to translate this data properly.
			// The data is also sent in-order and the first day is always the current day.
			Time dayTime = new Time();
			dayTime.setToNow();					// we start at the day returned by local time.
			int julianStartDay = Time.getJulianDay(System.currentTimeMillis(), dayTime.gmtoff);	// now we work exclusively in UTC
			dayTime = new Time();

			Vector<ContentValues> rowValues = new Vector<ContentValues>(weatherArray.length());
			for(int i = 0; i < weatherArray.length(); i++) {
				// These are the values that will be collected.
				long dateTime;
				double pressure;
				int humidity;
				double windSpeed;
				double windDirection;
				double high;
				double low;
				String description;
				int weatherId;

				// Get the JSON object representing the day
				dateTime = dayTime.setJulianDay(julianStartDay+i);
				JSONObject dayForecast = weatherArray.getJSONObject(i);
				pressure = dayForecast.getDouble(OWM_PRESSURE);
				humidity = dayForecast.getInt(OWM_HUMIDITY);
				windSpeed = dayForecast.getDouble(OWM_WINDSPEED);
				windDirection = dayForecast.getDouble(OWM_WIND_DIRECTION);

				// Description and weather ID are in a child array called "weather", which is 1 element long.
				JSONObject weatherObject = dayForecast.getJSONArray(OWM_WEATHER).getJSONObject(0);
				description = weatherObject.getString(OWM_DESCRIPTION);
				weatherId = weatherObject.getInt(OWM_WEATHER_ID);
				// Temperatures are in a child object called "temp".
				JSONObject temperatureObject = dayForecast.getJSONObject(OWM_TEMPERATURE);
				high = temperatureObject.getDouble(OWM_MAX);
				low = temperatureObject.getDouble(OWM_MIN);

				ContentValues weatherValues = new ContentValues();
				weatherValues.put(WeatherEntry.COLUMN_LOC_KEY, locationId);
				weatherValues.put(WeatherEntry.COLUMN_DATE, dateTime);
				weatherValues.put(WeatherEntry.COLUMN_HUMIDITY, humidity);
				weatherValues.put(WeatherEntry.COLUMN_PRESSURE, pressure);
				weatherValues.put(WeatherEntry.COLUMN_WIND_SPEED, windSpeed);
				weatherValues.put(WeatherEntry.COLUMN_DEGREES, windDirection);
				weatherValues.put(WeatherEntry.COLUMN_MAX_TEMP, high);
				weatherValues.put(WeatherEntry.COLUMN_MIN_TEMP, low);
				weatherValues.put(WeatherEntry.COLUMN_SHORT_DESC, description);
				weatherValues.put(WeatherEntry.COLUMN_CONDITION_ID, weatherId);
				rowValues.add(weatherValues);
			}
			int inserted = 0;
			int deleted  = 0;
			if ( rowValues.size() > 0 ) {
				ContentResolver provider = getContext().getContentResolver();				// bulkInsert new data to database;
				inserted = provider.bulkInsert(WeatherEntry.CONTENT_URI, rowValues.toArray(new ContentValues[rowValues.size()]));
				
				String yesterday = Long.toString(dayTime.setJulianDay(julianStartDay - 1)); // delete DB data older than yesterday;
				deleted = provider.delete(WeatherEntry.CONTENT_URI, WeatherEntry.COLUMN_DATE + " <= ? ", new String[] {yesterday});
				notifyWeather();		// show a Notification;
			}
			Log.v(LOG_TAG, "Syncing Completed: " + inserted + " Inserted.");
			Log.v(LOG_TAG, "Service Completed: " + deleted  + " Deleted.");
			Context context = getContext();
			SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(context).edit();
			editor.putLong(context.getString(R.string.last_update_key), System.currentTimeMillis());
			editor.commit();
		}
		catch (JSONException e) {			// cannot show Toast in service thread;
			Log.e(LOG_TAG, e.getMessage(), e);
			Context context = getContext();
			SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(context).edit();
			editor.putString(context.getString(R.string.service_error_key), context.getString(R.string.location_problem, locationSetting));
			editor.commit();
		}
	}
}