package com.soapcharts.sunshine.app;

import android.app.Activity;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.soapcharts.sunshine.app.data.WeatherContract.WeatherEntry;
import com.soapcharts.sunshine.app.sync.SunshineSyncAdapter;

import java.text.DateFormat;
import java.util.Date;

public class ForecastFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor>, SharedPreferences.OnSharedPreferenceChangeListener {
	private static final String LOG_TAG = ForecastFragment.class.getSimpleName();
	private static final String SELECTED_KEY = "Selection";

	// private ArrayAdapter<String> forecastAdapter;		// used for app wo. database;
	private ForecastAdapter forecastAdapter;
	private int listPosition = ListView.INVALID_POSITION;

	public interface Callback {				// Callback interface to be notified when an item is selected;
		public void onDateSelected(Uri dateUri);
	}

	public ForecastFragment() {				// MUST declare a public constructor
		super();
		setHasOptionsMenu(true);			// MUST ENABLE the options menu
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		getLoaderManager().initLoader(Utility.FORECAST_LOADER, null, this);
		super.onActivityCreated(savedInstanceState);
	}

	@Override
	public Loader<Cursor> onCreateLoader(int id, Bundle bundle) {
		String location = Utility.getPreferredLocation(getActivity());
		String sortOrder = WeatherEntry.COLUMN_DATE + " ASC ";
		Uri dataUri = WeatherEntry.buildWeatherLocationWithStartDate(location, System.currentTimeMillis());

		return new CursorLoader(getActivity(), dataUri, Utility.FORECAST_COLUMNS, null, null, sortOrder);
		// NOTE: CursorLoader enables the data to be loaded independent of the activity's lifecycle (ie: unaffected by screen rotation);
	}

	@Override
	public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
		forecastAdapter.swapCursor(cursor);                        // this causes CursorAdapter to update the UI;
		if (listPosition != ListView.INVALID_POSITION)
			((ListView) getActivity().findViewById(R.id.listview_forecast)).smoothScrollToPosition(listPosition);
	}

	@Override
	public void onLoaderReset(Loader<Cursor> loader) {
		forecastAdapter.swapCursor(null);
	}

	void onLocationChanged() {
		updateWeather();
	}

	// weather is updated periodically by SyncAdapter, when location is changed, or when Refresh is selected;
	private void updateWeather() {
		SunshineSyncAdapter.syncImmediately(getActivity());
		// WeatherFragment is updated also since it uses the same ContentProvider as this Fragment;

/*		// code below is for when SyncAdapter is not used;
		Context context = getActivity();
		Intent intent = new Intent(context, WeatherService.AlarmReceiver.class);
		intent.putExtra(WeatherService.LOCATION_EXTRA, Utility.getPreferredLocation(context));

		// A PendingIntent allows the caller to send data with the same permissions and app identity as the app that created it.
		PendingIntent alarm = PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_ONE_SHOT);
		AlarmManager manager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
		manager.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), alarm);		// alarm will trigger immediately;
*/
		// new FetchWeatherTask(getActivity(), forecastAdapter).execute(location);		// used for app wo. database;
		getLoaderManager().restartLoader(Utility.FORECAST_LOADER, null, this);			// originally in onLocationChanged;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// forecastAdapter = new ArrayAdapter<String>(getActivity(), R.layout.list_item_forecast, R.id.list_item_forecast, new ArrayList<String>());

		forecastAdapter = new ForecastAdapter(getActivity(), null, 0);						// flag == 0 since CursorLoader will load and sync the data;
		View rootView = inflater.inflate(R.layout.fragment_forecast_list, container, false);
		ListView view = (ListView) rootView.findViewById(R.id.listview_forecast);
		view.setOnItemClickListener(new AdapterView.OnItemClickListener() {@Override
						public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				// String forecast = (String) parent.getItemAtPosition(position);		// this code is used for app wo. database;
				// Intent intent = new Intent(getActivity(), DetailActivity.class).putExtra(Intent.EXTRA_TEXT, forecast);
				// startActivity(intent);

				Cursor cursor = (Cursor) parent.getItemAtPosition(position);
				if (cursor != null) {
					String location = Utility.getPreferredLocation(getActivity());
					Uri dateUri = WeatherEntry.buildWeatherLocationWithDate(location, cursor.getLong(Utility.COL_WEATHER_DATE));
					((Callback) getActivity()).onDateSelected(dateUri);
				}
				listPosition = position;	// inner class can reference outer class variables and methods;
						}
		});
		if (savedInstanceState != null && savedInstanceState.containsKey(SELECTED_KEY))
			listPosition = savedInstanceState.getInt(SELECTED_KEY);
		// updateWeather();				// used for app wo. database;
		view.setAdapter(forecastAdapter);
		return rootView;
	}

	@Override			// NOT WORKING: cannot pass Preference values between Service and Activity
	public void onSharedPreferenceChanged(SharedPreferences prefs, String key) {
		Activity context = getActivity();
		String lastErrorKey = getString(R.string.service_error_key);
		String lastError = prefs.getString(lastErrorKey, "");
		if (lastErrorKey.equals(key) && !lastError.isEmpty()) {
			Toast.makeText(context, lastError, Toast.LENGTH_LONG).show();
			SharedPreferences.Editor editor = prefs.edit();
			editor.remove(lastErrorKey);
			editor.apply();
		}
		String lastUpdateKey = getString(R.string.last_update_key);
		long time = prefs.getLong(lastUpdateKey, 0);
		if (lastUpdateKey.equals(key) && time > 0) {
			String date = DateFormat.getDateTimeInstance().format(new Date(time));
			((TextView) context.findViewById(R.id.last_updated)).setText(date);
			String location = Utility.getPreferredLocation(context);
			context.setTitle(getString(R.string.zipCode_weather, location));
		}
	}

	@Override
	public void onResume() {
		super.onResume();
		PreferenceManager.getDefaultSharedPreferences(getActivity()).registerOnSharedPreferenceChangeListener(this);
	}

	@Override
	public void onPause() {		// need to register and unregister as suggested in Settings guide;
		super.onPause();
		PreferenceManager.getDefaultSharedPreferences(getActivity()).unregisterOnSharedPreferenceChangeListener(this);
	}

	@Override
	// Android should save the selected position, but it doesn't for KitKat or lower API, so must keep track of listPosition;
	public void onSaveInstanceState(Bundle state) {
		if (listPosition != ListView.INVALID_POSITION)
			state.putInt(SELECTED_KEY, listPosition);
		super.onSaveInstanceState(state);
	}

	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.forecast, menu);
	}

	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.action_refresh) {
			updateWeather();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
