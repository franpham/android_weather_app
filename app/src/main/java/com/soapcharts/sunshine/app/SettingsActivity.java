package com.soapcharts.sunshine.app;

import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.widget.Toast;

/**
 * A {@link PreferenceActivity} that presents a set of application settings.
 * <p>
 * See <a href="http://developer.android.com/design/patterns/settings.html">
 * Android Design: Settings</a> for design guidelines and the <a
 * href="http://developer.android.com/guide/topics/ui/settings.html">Settings
 * API Guide</a> for more information on developing a Settings UI.
 */
public class SettingsActivity extends PreferenceActivity implements Preference.OnPreferenceChangeListener {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.pref_general);

		// For all preferences, attach an OnPreferenceChangeListener to update their Summary (description);
		Preference zipPreference = findPreference(getString(R.string.pref_location_key));
		Preference tempPreference= findPreference(getString(R.string.pref_temp_key));
		bindPreferenceSummaryToValue(zipPreference, tempPreference);
	}

	/**
	 * Attaches a listener so the summary is always updated with the preference value.
	 * Also fires the listener once, to initialize the summary to show up before the value is changed.
	 */
	private void bindPreferenceSummaryToValue(Preference... preferences) {
		// Set the listener to watch for value changes and trigger listener immediately with SAVED values;
		for (Preference pref : preferences) {
			pref.setOnPreferenceChangeListener(this);
			onPreferenceChange(pref, PreferenceManager.getDefaultSharedPreferences(pref.getContext()).getString(pref.getKey(), ""));
		}
	}

	@Override		// As stated in Android Settings Guide, the Preference's Summary is not automatically updated;
	public boolean onPreferenceChange(Preference preference, Object value) {
		String text = value.toString();
		if (preference instanceof ListPreference) {
			// For list preferences, look up the correct display value in the preference's 'entries' list.
			ListPreference listPreference = (ListPreference) preference;
			int prefIndex = listPreference.findIndexOfValue(text);
			if (prefIndex >= 0)
				preference.setSummary(listPreference.getEntries()[prefIndex]);
		}
		else if (preference instanceof EditTextPreference) {
			if (text.length() != 5) {
				Toast.makeText(this, getString(R.string.location_problem, text), Toast.LENGTH_SHORT).show();
				return false;
			}
			try {
				Integer.valueOf(text);
				preference.setSummary(text);
			} catch (NumberFormatException e) {
				return false;
			}
		}
		return true;
	}

	@TargetApi(Build.VERSION_CODES.JELLY_BEAN)		// method will only run in Jelly Bean or later;
	@Override										// fixes a bug where pressing Back creates a new Activity instead of going back to the previous one;
	public Intent getParentActivityIntent() {
		return super.getParentActivityIntent().addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
	}
}