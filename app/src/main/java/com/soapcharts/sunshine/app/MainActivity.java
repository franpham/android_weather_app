package com.soapcharts.sunshine.app;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;

import com.soapcharts.sunshine.app.sync.SunshineSyncAdapter;

/*
For phones: ForecastFragment resides in MainActivity and WeatherFragment resides in DetailActivity. A landscape layout is specified for WeatherFragment.
For tables: both ForecastFragment and WeatherFragment reside in MainActivity, so DetailActivity is never started. The layout is the same for either orientations.
*/
public class MainActivity extends ActionBarActivity implements ForecastFragment.Callback {
	private static final String DETAIL_FRAGMENT_TAG = "DETAIL_TAG";		// enables Fragment retrieval by tag;

	// mLocation cannot be static because it cannot persists between Activity's lifecycle;
	private String mLocation;			// set in onResume() after the user has changed the Settings;
	private boolean hasTwoPanes;		// flag to determine if the activity_main layout has 2 container;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		SunshineSyncAdapter.initializeSyncAdapter(this);

		mLocation = Utility.getPreferredLocation(this);
		if (findViewById(R.id.detail_container) != null) {		// The detail_container is only in activity_main.xml for tablets, which should be in two panes;
			if (savedInstanceState == null) {
				getSupportFragmentManager().beginTransaction()
						.replace(R.id.detail_container, new WeatherFragment(), DETAIL_FRAGMENT_TAG).commit();
				hasTwoPanes = true;            // only add the Fragment if Bundle is null because Android saves the UI state for screen rotations;
			} else {                           // else WeatherFragment and its layout are not needed, and ForecastFragment and its layout are statically added in xml;
				hasTwoPanes = false;
				getSupportActionBar().setElevation(0f);        // this will prevent the action bar to appear elevated on Lollipop version;

			}	// Tutorial difference: Today Item is the same for both phone & tablet so don't need mUseTodayLayout variable;
		}
	}

	@Override
	public void onDateSelected(Uri dateUri) {
		if (hasTwoPanes) {					// in tablet mode, just replace the WeatherFragment layout;
			Bundle args = new Bundle();
			args.putParcelable(WeatherFragment.DETAIL_URI, dateUri);
			WeatherFragment fragment = new WeatherFragment();
			fragment.setArguments(args);

			getSupportFragmentManager().beginTransaction()
					.replace(R.id.detail_container, fragment, DETAIL_FRAGMENT_TAG).commit();
		}
		else {		// in phone mode, create a new Intent to create the DetailActivity;
			Intent intent = new Intent(this, DetailActivity.class).setData(dateUri);
			startActivity(intent);
		}
	}

	// NOTE: location changes are monitored in this class (instead of SettingsActivity) because updateWeather() requires the relevant Activity context.
	@Override				// location changes are monitored in Activity instead of Fragment since Activity is aware of the affected Fragments;
	protected void onResume() {
		super.onResume();
		String locationSetting = Utility.getPreferredLocation(this);
		if (mLocation.equals(locationSetting))
			return;
		mLocation = locationSetting;					// ForecastFragment always exists in MainActivity since its statically added;
		ForecastFragment forecast = (ForecastFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_forecast);
		forecast.onLocationChanged();
		WeatherFragment weather = (WeatherFragment) getSupportFragmentManager().findFragmentByTag(DETAIL_FRAGMENT_TAG);
		if (weather != null)
			weather.onLocationChanged(mLocation);		// WeatherFragment exists in MainActivity only for tablets (see onCreate above)
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override		// Activities handle general MenuItems, while Fragment should handle content specific MenuItems;
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			Intent intent = new Intent(this, SettingsActivity.class);
			startActivity(intent);
			return true;
		}
		else if (id == R.id.action_map) {
			Uri location = Utility.getGeoLocation(this);
			Intent intent = new Intent(Intent.ACTION_VIEW, location);
			if (intent.resolveActivity(getPackageManager()) != null)
				startActivity(intent);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
