package com.soapcharts.sunshine.app;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * {@link ForecastAdapter} exposes a list of weather forecasts
 * from a {@link android.database.Cursor} to a {@link android.widget.ListView}.
 */
public class ForecastAdapter extends CursorAdapter {
	// when Cursor is null, CursorAdapter uses context.getContentResolver to get a Cursor, which was set by CursorLoader;
	// the adapter will register a ContentObserver on its Cursor, for which the ContentProvider will notify of changes;

	// IMPORTANT: CursorAdapter has references to the current Cursor (mCursor) and Context (mContext);
	private static final int VIEW_TYPE_TODAY = 0;		// NOTE: VIEW_TYPE must start at 0!
	private static final int VIEW_TYPE_FUTURE= 1;
	private static final int VIEW_TYPE_COUNT = 2;

	public ForecastAdapter(Context context, Cursor c, int flags) {
		super(context, c, flags);
	}

	@Override
	public int getViewTypeCount() {
		return VIEW_TYPE_COUNT;
	}

	@Override	// Tutorial difference: Today Item is the same for both phone & tablet so don't need mUseTodayLayout variable;
	public int getItemViewType(int position) {
		return position == 0 ? VIEW_TYPE_TODAY : VIEW_TYPE_FUTURE;
	}

	/*
		Remember that these views are reused as needed. IMPORTANT: override newView and bindView
	 */
	@Override
	public View newView(Context context, Cursor cursor, ViewGroup parent) {
		int viewType = getItemViewType(cursor.getPosition());
		int layoutId = viewType == VIEW_TYPE_TODAY ? R.layout.list_item_today : R.layout.list_item_forecast;
		View rootView = LayoutInflater.from(context).inflate(layoutId, parent, false);
		ViewHolder holder = new ViewHolder(rootView);
		rootView.setTag(holder);
		return rootView;
	}

	/*
		This is where we fill-in the views with the contents of the cursor.
	 */
	@Override
	public void bindView(View view, Context context, Cursor cursor) {
		ViewHolder holder = (ViewHolder) view.getTag();
		String desc = cursor.getString(Utility.COL_WEATHER_DESC);
		TextView textView = holder.dateView;
		textView.setText(Utility.getFormattedDate(context, cursor.getLong(Utility.COL_WEATHER_DATE)));
		textView = holder.forecastView;
		textView.setText(desc);
		textView = holder.highTempView;
		textView.setText(Utility.formatTemperature(context, cursor.getDouble(Utility.COL_WEATHER_MAX_TEMP)));
		textView = holder.lowTempView;
		textView.setText(Utility.formatTemperature(context, cursor.getDouble(Utility.COL_WEATHER_MIN_TEMP)));
		ImageView imageView = holder.iconView;
		int imageId = cursor.getInt(Utility.COL_CONDITION_ID);
		imageId = getItemViewType(cursor.getPosition()) == VIEW_TYPE_TODAY ? Utility.getArtForWeatherId(imageId)
					: Utility.getIconForWeatherId(imageId);
		imageView.setImageResource(imageId);
		imageView.setContentDescription(desc);
	}

	/**
	 * Cache of the children views for a forecast list item.
	 */
	public static class ViewHolder {
		final ImageView iconView;
		final TextView dateView;
		final TextView forecastView;
		final TextView highTempView;
		final TextView lowTempView;

		public ViewHolder(View view) {
			iconView = (ImageView) view.findViewById(R.id.list_item_icon);
			dateView = (TextView) view.findViewById(R.id.list_item_date);
			forecastView = (TextView) view.findViewById(R.id.list_item_forecast);
			highTempView = (TextView) view.findViewById(R.id.list_item_high);
			lowTempView = (TextView) view.findViewById(R.id.list_item_low);
		}
	}
}