package com.soapcharts.sunshine.app;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.ShareActionProvider;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.soapcharts.sunshine.app.data.WeatherContract.WeatherEntry;

// If Fragment is an inner class, it MUST BE static since non-static classes hold reference to the Activity parent, which causes memory leak.
public class WeatherFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {
	private static final String WEATHER_LOG_TAG = WeatherFragment.class.getSimpleName();
	static final String DETAIL_URI = "DATE_URI";

	private ViewHolder holder;				// this Fragment and its layout are statically created (declared in xml) for phone and tablet;
	private String weatherDetails;			// the details for Share action; this is set when CursorLoader has finished loading;
	private Uri dateUri;					// the date Uri passed by ItemClickListener in ForecastFragment when user selects a date;
	private Uri cityUri;					// the Uri with coordinates for the Map action;

	public WeatherFragment() {				// MUST declare a public constructor
		super();
		setHasOptionsMenu(true);			// MUST ENABLE the options menu
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		getLoaderManager().initLoader(Utility.FORECAST_LOADER, null, this);
		super.onActivityCreated(savedInstanceState);
	}

	@Override
	public Loader<Cursor> onCreateLoader(int id, Bundle bundle) {
		// both DetailActivity and MainActivity pass a date Uri;
		return new CursorLoader(getActivity(),dateUri, Utility.FORECAST_COLUMNS, null, null, null);
	}

	@Override
	public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
		if (cursor != null && !cursor.moveToFirst())
			return;
		Context context = getActivity();
		String date = Utility.getFormattedDate(context, cursor.getLong(Utility.COL_WEATHER_DATE));
		String desc = cursor.getString(Utility.COL_WEATHER_DESC);
		String high = Utility.formatTemperature(context, cursor.getDouble(Utility.COL_WEATHER_MAX_TEMP));
		String low  = Utility.formatTemperature(context, cursor.getDouble(Utility.COL_WEATHER_MIN_TEMP));
		holder.dateView.setText(date);
		holder.forecastView.setText(desc);
		holder.highTempView.setText(context.getString(R.string.high_temperature, high));
		holder.lowTempView.setText(context.getString(R.string.low_temperature, low));
		holder.humidityView.setText(context.getString(R.string.format_humidity, cursor.getFloat(Utility.COL_HUMIDITY)));
		holder.pressureView.setText(context.getString(R.string.format_pressure, cursor.getFloat(Utility.COL_PRESSURE)));
		holder.windView.setText(Utility.getFormattedWind(context, cursor.getFloat(Utility.COL_WIND_SPEED), cursor.getFloat(Utility.COL_WIND_DEGREES)));
		holder.iconView.setImageResource(Utility.getArtForWeatherId(cursor.getInt(Utility.COL_CONDITION_ID)));
		holder.iconView.setContentDescription(desc);
		cityUri = Utility.getGeoLocation(cursor);
		weatherDetails = context.getString(R.string.format_notification, desc, high, low);
		createForecastIntent();							// call after setting weatherDetails;
	}

	@Override
	public void onLoaderReset(Loader<Cursor> loader) {
		// must declare, but do nothing;
	}

	void onLocationChanged(String newLocation) {		// do not fetch new weather, which is done by ForecastFragment;
		if (dateUri == null)
			return;
		long date = WeatherEntry.getDateFromUri(dateUri);
		dateUri = WeatherEntry.buildWeatherLocationWithDate(newLocation, date);
		getLoaderManager().restartLoader(Utility.FORECAST_LOADER, null, this);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		Bundle args = getArguments();
		if (args != null)
			dateUri = args.getParcelable(DETAIL_URI);
		View rootView = inflater.inflate(R.layout.fragment_weather, container, false);
		holder = new ViewHolder(rootView);
		return rootView;
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.weather, menu);
		MenuItem item = menu.findItem(R.id.action_share);
		holder.actionProvider = (ShareActionProvider) MenuItemCompat.getActionProvider(item);
		createForecastIntent();				// using support.v7; item.getActionProvider is not supported till API 14;
	}
	// for ActionProvider, set the Intent in onCreateOptionsMenu instead of when onOptionsItemSelected is called;

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.action_map) {
			Intent intent = new Intent(Intent.ACTION_VIEW, cityUri);
			if (cityUri != null && intent.resolveActivity(getActivity().getPackageManager()) != null)
				startActivity(intent);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	// weatherDetails is set in onLoadFinished and actionProvider is created in onCreateOptionsMenu (via MenuInflater), which occurs ASYNCHRONOUSLY;
	private void createForecastIntent() {
		if (weatherDetails == null || holder.actionProvider == null)
			return;
		Intent intent = new Intent(Intent.ACTION_SEND);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
		// the above flag is needed so that the receiving activity is not put on the activity stack, which would affect your app's back action.
		// the above flag is equivalent to FLAG_ACTIVITY_NEW_DOCUMENT, which is the preferred flag as of API 21.

		intent.setType("text/plain");                    // set the MIME type;
		intent.putExtra(Intent.EXTRA_TEXT, weatherDetails + " #SunshineApp");
		holder.actionProvider.setShareIntent(intent);
	}

	/**
	 * Cache of the children views for the Detail view;
	 */
	public static class ViewHolder {
		final ImageView iconView;
		final TextView dateView;
		final TextView forecastView;
		final TextView highTempView;
		final TextView lowTempView;
		final TextView humidityView;
		final TextView windView;
		final TextView pressureView;
		ShareActionProvider actionProvider;	// ActionProvider for Share action; this is set when menu is created by MenuInflater (via xml);


		public ViewHolder(View view) {
			iconView = (ImageView) view.findViewById(R.id.detail_icon);
			dateView = (TextView) view.findViewById(R.id.detail_date);
			forecastView = (TextView) view.findViewById(R.id.detail_forecast);
			highTempView = (TextView) view.findViewById(R.id.detail_high);
			lowTempView = (TextView) view.findViewById(R.id.detail_low);
			humidityView = (TextView) view.findViewById(R.id.detail_humidity);
			windView = (TextView) view.findViewById(R.id.detail_wind);
			pressureView = (TextView) view.findViewById(R.id.detail_pressure);
		}
	}
}
