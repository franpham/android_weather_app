package com.soapcharts.sunshine.app.service;

import android.app.IntentService;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.text.format.Time;
import android.util.Log;

import com.soapcharts.sunshine.app.data.WeatherContract;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Vector;

public class WeatherService extends IntentService {
	private static final String LOG_TAG = WeatherService.class.getSimpleName();
	public static final String LOCATION_EXTRA = "location_extra";

	public WeatherService() {
		super("WeatherService");
	}
	// IntentService will run independent of the Activity's lifecycle, but it does not manage network calls or sync data with ContentProvider like SyncAdapters.

	public static class AlarmReceiver extends BroadcastReceiver {		// class MUST BE static;

		@Override
		public void onReceive(Context context, Intent receivedIntent) {
			// must create a new Intent since the targeted class needs to be WeatherService, not AlarmReceiver;
			Intent sentIntent = new Intent(context, WeatherService.class);
			sentIntent.putExtra(LOCATION_EXTRA, receivedIntent.getStringExtra(LOCATION_EXTRA));
			context.startService(sentIntent);
		}
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		String location = intent.getStringExtra(LOCATION_EXTRA);
		String format = "json";
		String units = "metric";	// convert units locally so network is not queried again if units are changed;

		// These two need to be declared outside the try/catch so that they can be closed in the finally block.
		HttpURLConnection urlConnection = null;
		BufferedReader reader = null;
		try {
			// Possible parameters are avaiable at OWM's forecast API page, at http://openweathermap.org/API#forecast
			final String FORECAST_BASE_URL = "http://api.openweathermap.org/data/2.5/forecast/daily?";
			final String QUERY_PARAM = "q";
			final String FORMAT_PARAM = "mode";
			final String UNITS_PARAM = "units";
			final String DAYS_PARAM = "cnt";

			// NOTE: website does not recognize all USA zipcodes, ie: 92014 returns an error (view link directly);
			Uri builtUri = Uri.parse(FORECAST_BASE_URL).buildUpon()				// add USA (not US) for country;
					.appendQueryParameter(QUERY_PARAM, location + ",USA")
					.appendQueryParameter(FORMAT_PARAM, format)
					.appendQueryParameter(UNITS_PARAM, units)
					.appendQueryParameter(DAYS_PARAM, "14").build();
			URL url = new URL(builtUri.toString());

			// Create the request to OpenWeatherMap, and open the connection
			urlConnection = (HttpURLConnection) url.openConnection();
			urlConnection.setRequestMethod("GET");
			urlConnection.connect();
			InputStream inputStream = urlConnection.getInputStream();
			if (inputStream == null)
				return;

			String line;
			StringBuffer buffer = new StringBuffer();
			reader = new BufferedReader(new InputStreamReader(inputStream));
			while ((line = reader.readLine()) != null) {
				// Since it's JSON, adding a newline isn't necessary (it won't affect parsing)
				// But it does make debugging a *lot* easier if you print out the completed buffer for debugging.
				buffer.append(line).append("\n");
			}
			if (buffer.length() == 0)
				return;		// Stream was empty. No point in parsing.

			String forecastJsonStr = buffer.toString();
			saveWeatherDataFromJson(forecastJsonStr, location);
		}
		catch (IOException e) {
			Log.e(LOG_TAG, "Error ", e);
		}
		finally {
			if (urlConnection != null) {
				urlConnection.disconnect();
			}
			if (reader != null) {
				try {
					reader.close();
				} catch (final IOException e) {
					Log.e(LOG_TAG, "Error closing stream", e);
				}
			}
		}
	}

	/**
	 * Helper method to handle insertion of a new location in the weather database.
	 * @param locationSetting The location string used to request updates from the server.
	 * @param cityName A human-readable city name, e.g "Mountain View"
	 * @param lat the latitude of the city
	 * @param lon the longitude of the city
	 * @return the row ID of the added location.
	 */
	long addLocation(String locationSetting, String cityName, double lat, double lon) {
		long rowId;
		ContentResolver provider = getContentResolver();
		Cursor cursor = provider.query(WeatherContract.LocationEntry.CONTENT_URI, null,
				WeatherContract.LocationEntry.COLUMN_LOCATION_SETTING + " = ? ", new String [] {locationSetting}, null);
		if (cursor.moveToFirst())
			rowId = cursor.getLong(cursor.getColumnIndex(WeatherContract.LocationEntry._ID));
		else {
			ContentValues values = new ContentValues();
			values.put(WeatherContract.LocationEntry.COLUMN_LOCATION_SETTING, locationSetting);
			values.put(WeatherContract.LocationEntry.COLUMN_CITY_NAME, cityName);
			values.put(WeatherContract.LocationEntry.COLUMN_COORD_LAT, lat);
			values.put(WeatherContract.LocationEntry.COLUMN_COORD_LONG, lon);
			// Java 5 automatically converts primitive types to Objects;
			Uri newUri = provider.insert(WeatherContract.LocationEntry.CONTENT_URI, values);
			rowId = ContentUris.parseId(newUri);
		}
		cursor.close();
		return rowId;
	}

	/**
	 * Take the String representing the complete forecast in JSON Format and
	 * pull out the data we need to construct the Strings needed for the wireframes.
	 */
	private void saveWeatherDataFromJson(String forecastJsonStr, String locationSetting) {
		// These are the names of the JSON objects that need to be extracted.

		final String OWM_CITY = "city";
		final String OWM_CITY_NAME = "name";
		final String OWM_COORD = "coord";
		final String OWM_LATITUDE = "lat";
		final String OWM_LONGITUDE = "lon";
		// Weather information.  Each day's forecast info is an element of the "list" array.
		final String OWM_LIST = "list";
		final String OWM_PRESSURE = "pressure";
		final String OWM_HUMIDITY = "humidity";
		final String OWM_WINDSPEED = "speed";
		final String OWM_WIND_DIRECTION = "deg";
		// All temperatures are children of the "temp" object.
		final String OWM_TEMPERATURE = "temp";
		final String OWM_MAX = "max";
		final String OWM_MIN = "min";
		final String OWM_WEATHER = "weather";
		final String OWM_DESCRIPTION = "main";
		final String OWM_WEATHER_ID = "id";
		try {
			JSONObject forecastJson = new JSONObject(forecastJsonStr);
			JSONArray weatherArray = forecastJson.getJSONArray(OWM_LIST);
			JSONObject cityJson = forecastJson.getJSONObject(OWM_CITY);
			String cityName = cityJson.getString(OWM_CITY_NAME);
			JSONObject cityCoord = cityJson.getJSONObject(OWM_COORD);
			double cityLatitude = cityCoord.getDouble(OWM_LATITUDE);
			double cityLongitude = cityCoord.getDouble(OWM_LONGITUDE);

			long locationId = addLocation(locationSetting, cityName, cityLatitude, cityLongitude);
			Log.v(LOG_TAG, "Location added/ updated: " + cityName + ", ID: " + locationId);
			// IMPORTANT: add the location to DB if it doesn't exist;

			// OWM returns daily forecasts based upon the local time of the city that is being
			// asked for, which means that we need to know the GMT offset to translate this data properly.
			// The data is also sent in-order and the first day is always the current day.
			Time dayTime = new Time();
			dayTime.setToNow();					// we start at the day returned by local time.
			int julianStartDay = Time.getJulianDay(System.currentTimeMillis(), dayTime.gmtoff);	// now we work exclusively in UTC
			dayTime = new Time();

			Vector<ContentValues> cVVector = new Vector<ContentValues>(weatherArray.length());
			for(int i = 0; i < weatherArray.length(); i++) {
				// These are the values that will be collected.
				long dateTime;
				double pressure;
				int humidity;
				double windSpeed;
				double windDirection;
				double high;
				double low;
				String description;
				int weatherId;

				// Get the JSON object representing the day
				dateTime = dayTime.setJulianDay(julianStartDay+i);
				JSONObject dayForecast = weatherArray.getJSONObject(i);
				pressure = dayForecast.getDouble(OWM_PRESSURE);
				humidity = dayForecast.getInt(OWM_HUMIDITY);
				windSpeed = dayForecast.getDouble(OWM_WINDSPEED);
				windDirection = dayForecast.getDouble(OWM_WIND_DIRECTION);

				// Description and weather ID are in a child array called "weather", which is 1 element long.
				JSONObject weatherObject = dayForecast.getJSONArray(OWM_WEATHER).getJSONObject(0);
				description = weatherObject.getString(OWM_DESCRIPTION);
				weatherId = weatherObject.getInt(OWM_WEATHER_ID);
				// Temperatures are in a child object called "temp".
				JSONObject temperatureObject = dayForecast.getJSONObject(OWM_TEMPERATURE);
				high = temperatureObject.getDouble(OWM_MAX);
				low = temperatureObject.getDouble(OWM_MIN);

				ContentValues weatherValues = new ContentValues();
				weatherValues.put(WeatherContract.WeatherEntry.COLUMN_LOC_KEY, locationId);
				weatherValues.put(WeatherContract.WeatherEntry.COLUMN_DATE, dateTime);
				weatherValues.put(WeatherContract.WeatherEntry.COLUMN_HUMIDITY, humidity);
				weatherValues.put(WeatherContract.WeatherEntry.COLUMN_PRESSURE, pressure);
				weatherValues.put(WeatherContract.WeatherEntry.COLUMN_WIND_SPEED, windSpeed);
				weatherValues.put(WeatherContract.WeatherEntry.COLUMN_DEGREES, windDirection);
				weatherValues.put(WeatherContract.WeatherEntry.COLUMN_MAX_TEMP, high);
				weatherValues.put(WeatherContract.WeatherEntry.COLUMN_MIN_TEMP, low);
				weatherValues.put(WeatherContract.WeatherEntry.COLUMN_SHORT_DESC, description);
				weatherValues.put(WeatherContract.WeatherEntry.COLUMN_CONDITION_ID, weatherId);
				cVVector.add(weatherValues);
			}
			int inserted = 0;
			if ( cVVector.size() > 0 )
				inserted = getContentResolver().bulkInsert(				// IMPORTANT: bulkInsert new data to database;
						WeatherContract.WeatherEntry.CONTENT_URI, cVVector.toArray(new ContentValues[cVVector.size()]));
			Log.v(LOG_TAG, "WeatherService Complete: " + inserted + " Inserted");

		}
		catch (JSONException e) {
			Log.e(LOG_TAG, e.getMessage(), e);
			e.printStackTrace();
		}
	}
}
