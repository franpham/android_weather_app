/*
 * Copyright (C) 2014 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.soapcharts.sunshine.app;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.text.format.Time;
import android.util.Log;
import android.widget.ArrayAdapter;

import com.soapcharts.sunshine.app.data.WeatherContract.LocationEntry;
import com.soapcharts.sunshine.app.data.WeatherContract.WeatherEntry;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Vector;

	// The 3 parameters for AsyncTask are given to: doInBackground, onProgressUpdate, and onPostExecute;
	// To keep this class compatible with and without a database, keep String[] as the Result and check for null on onPostExecute;
public class FetchWeatherTask extends AsyncTask<String, Void, String[]> {
	private final String LOG_TAG = FetchWeatherTask.class.getSimpleName();
	private ArrayAdapter<String> forecastArrayAdapter;		// used for app wo. database, or app with db but wo. CursorAdapter;
	private final Context mContext;

	public FetchWeatherTask(Context context) {
		mContext = context;
	}
	// AsyncTask enables background tasks to be done on a non-UI thread, but it's still bound to the Activity's lifecycle;

	public FetchWeatherTask(Context context, ArrayAdapter<String> forecastAdapter) {
		mContext = context;
		forecastArrayAdapter = forecastAdapter;
	}

		/**
	 * Helper method to handle insertion of a new location in the weather database.
	 * @param locationSetting The location string used to request updates from the server.
	 * @param cityName A human-readable city name, e.g "Mountain View"
	 * @param lat the latitude of the city
	 * @param lon the longitude of the city
	 * @return the row ID of the added location.
	 */
	long addLocation(String locationSetting, String cityName, double lat, double lon) {
		long rowId;
		ContentResolver provider = mContext.getContentResolver();
		Cursor cursor = provider.query(LocationEntry.CONTENT_URI, null, LocationEntry.COLUMN_LOCATION_SETTING + " = ? ", new String [] {locationSetting}, null);
		if (cursor.moveToFirst())
			rowId = cursor.getLong(cursor.getColumnIndex(LocationEntry._ID));
		else {
			ContentValues values = new ContentValues();
			values.put(LocationEntry.COLUMN_LOCATION_SETTING, locationSetting);
			values.put(LocationEntry.COLUMN_CITY_NAME, cityName);
			values.put(LocationEntry.COLUMN_COORD_LAT, lat);
			values.put(LocationEntry.COLUMN_COORD_LONG, lon);
			// Java 5 automatically converts primitive types to Objects;
			Uri newUri = provider.insert(LocationEntry.CONTENT_URI, values);
			rowId = ContentUris.parseId(newUri);
		}
		cursor.close();
		return rowId;
	}

	/**
	 * Take the String representing the complete forecast in JSON Format and
	 * pull out the data we need to construct the Strings needed for the wireframes.
	 */
	   private String [] getWeatherDataFromJson(String forecastJsonStr, String locationSetting) {
		// These are the names of the JSON objects that need to be extracted.

		final String OWM_CITY = "city";
		final String OWM_CITY_NAME = "name";
		final String OWM_COORD = "coord";
		final String OWM_LATITUDE = "lat";
		final String OWM_LONGITUDE = "lon";
		// Weather information.  Each day's forecast info is an element of the "list" array.
		final String OWM_LIST = "list";
		final String OWM_PRESSURE = "pressure";
		final String OWM_HUMIDITY = "humidity";
		final String OWM_WINDSPEED = "speed";
		final String OWM_WIND_DIRECTION = "deg";
		// All temperatures are children of the "temp" object.
		final String OWM_TEMPERATURE = "temp";
		final String OWM_MAX = "max";
		final String OWM_MIN = "min";
		final String OWM_WEATHER = "weather";
		final String OWM_DESCRIPTION = "main";
		final String OWM_WEATHER_ID = "id";
		try {
			JSONObject forecastJson = new JSONObject(forecastJsonStr);
			JSONArray weatherArray = forecastJson.getJSONArray(OWM_LIST);
			JSONObject cityJson = forecastJson.getJSONObject(OWM_CITY);
			String cityName = cityJson.getString(OWM_CITY_NAME);
			JSONObject cityCoord = cityJson.getJSONObject(OWM_COORD);
			double cityLatitude = cityCoord.getDouble(OWM_LATITUDE);
			double cityLongitude = cityCoord.getDouble(OWM_LONGITUDE);

			long locationId = addLocation(locationSetting, cityName, cityLatitude, cityLongitude);
			// IMPORTANT: add the location to DB if it doesn't exist;

			// OWM returns daily forecasts based upon the local time of the city that is being
			// asked for, which means that we need to know the GMT offset to translate this data properly.
			// The data is also sent in-order and the first day is always the current day.
			Time dayTime = new Time();
			dayTime.setToNow();					// we start at the day returned by local time.
			int julianStartDay = Time.getJulianDay(System.currentTimeMillis(), dayTime.gmtoff);	// now we work exclusively in UTC
			dayTime = new Time();

			Vector<ContentValues> cVVector = new Vector<ContentValues>(weatherArray.length());
			for(int i = 0; i < weatherArray.length(); i++) {
				// These are the values that will be collected.
				long dateTime;
				double pressure;
				int humidity;
				double windSpeed;
				double windDirection;
				double high;
				double low;
				String description;
				int weatherId;

				// Get the JSON object representing the day
				dateTime = dayTime.setJulianDay(julianStartDay+i);
				JSONObject dayForecast = weatherArray.getJSONObject(i);
				pressure = dayForecast.getDouble(OWM_PRESSURE);
				humidity = dayForecast.getInt(OWM_HUMIDITY);
				windSpeed = dayForecast.getDouble(OWM_WINDSPEED);
				windDirection = dayForecast.getDouble(OWM_WIND_DIRECTION);

				// Description and weather ID are in a child array called "weather", which is 1 element long.
				JSONObject weatherObject = dayForecast.getJSONArray(OWM_WEATHER).getJSONObject(0);
				description = weatherObject.getString(OWM_DESCRIPTION);
				weatherId = weatherObject.getInt(OWM_WEATHER_ID);
				// Temperatures are in a child object called "temp".
				JSONObject temperatureObject = dayForecast.getJSONObject(OWM_TEMPERATURE);
				high = temperatureObject.getDouble(OWM_MAX);
				low = temperatureObject.getDouble(OWM_MIN);

				ContentValues weatherValues = new ContentValues();
				weatherValues.put(WeatherEntry.COLUMN_LOC_KEY, locationId);
				weatherValues.put(WeatherEntry.COLUMN_DATE, dateTime);
				weatherValues.put(WeatherEntry.COLUMN_HUMIDITY, humidity);
				weatherValues.put(WeatherEntry.COLUMN_PRESSURE, pressure);
				weatherValues.put(WeatherEntry.COLUMN_WIND_SPEED, windSpeed);
				weatherValues.put(WeatherEntry.COLUMN_DEGREES, windDirection);
				weatherValues.put(WeatherEntry.COLUMN_MAX_TEMP, high);
				weatherValues.put(WeatherEntry.COLUMN_MIN_TEMP, low);
				weatherValues.put(WeatherEntry.COLUMN_SHORT_DESC, description);
				weatherValues.put(WeatherEntry.COLUMN_CONDITION_ID, weatherId);
				cVVector.add(weatherValues);
			}
			int inserted = 0;
			if ( cVVector.size() > 0 )
				inserted = mContext.getContentResolver().bulkInsert(		// IMPORTANT: bulkInsert new data to database;
						WeatherEntry.CONTENT_URI, cVVector.toArray(new ContentValues[cVVector.size()]));
			Log.d(LOG_TAG, "FetchWeatherTask Complete. " + inserted + " Inserted");
			return new String[0];		// return empty array since null indicates parsing failure;

/*			// uncomment code below if using ArrayAdapter instead of CursorAdapter, or if testing database inserts;
			// to read values, use DatabaseUtils.cursorRowToContentValues to store all contents of a row in ContentValues;

			String sortOrder = WeatherEntry.COLUMN_DATE + " ASC ";
			Uri dataUri = WeatherEntry.buildWeatherLocationWithStartDate(locationSetting, System.currentTimeMillis());
			Cursor cursor = mContext.getContentResolver().query(dataUri, null, null, null, sortOrder);
			String [] values = new String[cursor.getCount()];
			if (cursor.moveToFirst()) {
				for (int i = 0; i < values.length; i++) {
					values[i] = Utility.convertCursorRowToUXFormat(mContext, cursor);
					cursor.moveToNext();
				}
			}
			cursor.close();
			return values;
			*/
		}
		catch (JSONException e) {
			Log.e(LOG_TAG, e.getMessage(), e);
			e.printStackTrace();
			return null;
		}
	}

	@Override		// the return value of doInBackground is given to onPostExecute;
	protected String[] doInBackground(String... params) {
		// If there's no zip code, there's nothing to look up.  Verify size of params.
		if (params.length == 0)
			return null;
		String locationQuery = params[0];
		String format = "json";
		String units = "metric";	// convert units locally so network is not queried again if units are changed;

		// These two need to be declared outside the try/catch so that they can be closed in the finally block.
		HttpURLConnection urlConnection = null;
		BufferedReader reader = null;
		try {
			// Possible parameters are avaiable at OWM's forecast API page, at http://openweathermap.org/API#forecast
			final String FORECAST_BASE_URL = "http://api.openweathermap.org/data/2.5/forecast/daily?";
			final String QUERY_PARAM = "q";
			final String FORMAT_PARAM = "mode";
			final String UNITS_PARAM = "units";
			final String DAYS_PARAM = "cnt";

			// NOTE: website does not recognize all USA zipcodes, ie: 92014 returns an error (view link directly);
			Uri builtUri = Uri.parse(FORECAST_BASE_URL).buildUpon()		// add USA (not US) for country;
					.appendQueryParameter(QUERY_PARAM, locationQuery + ",USA")
					.appendQueryParameter(FORMAT_PARAM, format)
					.appendQueryParameter(UNITS_PARAM, units)
					.appendQueryParameter(DAYS_PARAM, "14").build();
			URL url = new URL(builtUri.toString());

			// Create the request to OpenWeatherMap, and open the connection
			urlConnection = (HttpURLConnection) url.openConnection();
			urlConnection.setRequestMethod("GET");
			urlConnection.connect();
			InputStream inputStream = urlConnection.getInputStream();
			if (inputStream == null)
				return null;

			String line;
			StringBuffer buffer = new StringBuffer();
			reader = new BufferedReader(new InputStreamReader(inputStream));
			while ((line = reader.readLine()) != null) {
				// Since it's JSON, adding a newline isn't necessary (it won't affect parsing)
				// But it does make debugging a *lot* easier if you print out the completed buffer for debugging.
				buffer.append(line).append("\n");
			}
			if (buffer.length() == 0)
				return null;		// Stream was empty. No point in parsing.

			String forecastJsonStr = buffer.toString();
			return getWeatherDataFromJson(forecastJsonStr, locationQuery);	// data is saved in this method;
		}
		catch (IOException e) {
			Log.e(LOG_TAG, "Error ", e);
			return null;		// network problem;
		}
		finally {
			if (urlConnection != null) {
				urlConnection.disconnect();
			}
			if (reader != null) {
				try {
					reader.close();
				} catch (final IOException e) {
					Log.e(LOG_TAG, "Error closing stream", e);
				}
			}
		}
	}

	@Override
	// do nothing if CursorAdapter is used since ContentProvider will notify adapters of content changes;
	protected void onPostExecute(String[] result) {
		// must check for null: result == null if network or parsing error; ArrayAdapter == null if using CursorAdapter;
		if (result != null && forecastArrayAdapter != null) {
			forecastArrayAdapter.clear();
			for(String dayForecastStr : result) {
				forecastArrayAdapter.add(dayForecastStr);
			}
		}
	}

		//region AsyncTask method that doesn't save the data in the database;
/*  AsyncTask are tied to their activities, which can be destroyed whenever the screen is rotated

		*//**
		 * Take the String representing the complete forecast in JSON Format and
		 * pull out the data we need to construct the Strings needed for the wireframes.
		 *//*
		private String[] getWeatherDataFromJson(String forecastJsonStr, int numDays) {

			// These are the names of the JSON objects that need to be extracted.
			final String OWM_LIST = "list";
			final String OWM_WEATHER = "weather";
			final String OWM_TEMPERATURE = "temp";
			final String OWM_MAX = "max";
			final String OWM_MIN = "min";
			final String OWM_DESCRIPTION = "main";
			final String DATA_NOT_READABLE = "Data for this date is not readable.";
			final String DATA_NOT_OBTAINED = "Data for this location could not be obtained.";

			// OWM returns daily forecasts based upon the local time of the city that is being
			// asked for, which means that we need to know the GMT offset to translate this data properly.
			// Since this data is also sent in-order and the first day is always the
			// current day, we're going to take advantage of that to get a nice
			// normalized UTC date for all of our weather.

			Time dayTime = new Time();
			dayTime.setToNow();
			int julianStartDay = Time.getJulianDay(System.currentTimeMillis(), dayTime.gmtoff);
			dayTime = new Time();					// now we work exclusively in UTC

			JSONArray weatherArray;
			String [] resultStrs;
			try {

				JSONObject forecastJson = new JSONObject(forecastJsonStr);
				weatherArray = forecastJson.getJSONArray(OWM_LIST);
				resultStrs = new String[numDays];
			}
			catch (JSONException e) {
				Log.e(FWT_LOG_TAG, DATA_NOT_OBTAINED);
				resultStrs = new String[] {DATA_NOT_OBTAINED};
				return resultStrs;
			}
			for(int i = 0; i < weatherArray.length(); i++) {
				try {
					String description, highAndLow;

					// Get the JSON object representing the day and temperatures
					JSONObject dayForecast = weatherArray.getJSONObject(i);
					JSONObject temperatureObject = dayForecast.getJSONObject(OWM_TEMPERATURE);

					// The date/time is returned as a long.  We need to convert that into something human-readable
					long dateTime = dayTime.setJulianDay(julianStartDay + i);
					String day = Utility.getFormattedDate(mContext, dateTime);

					// description is in a child array called "weather", which is 1 element long.
					JSONObject weatherObject = dayForecast.getJSONArray(OWM_WEATHER).getJSONObject(0);
					description = weatherObject.getString(OWM_DESCRIPTION);

					// Temperatures are in a child object called "temp".
					double high = temperatureObject.getDouble(OWM_MAX);
					double low = temperatureObject.getDouble(OWM_MIN);
					highAndLow = Utility.formatHighLows(high, low, Utility.isMetric(mContext));
					resultStrs[i] = day + " - " + description + " - " + highAndLow;
				}
				catch (JSONException e) {
					resultStrs[i] = DATA_NOT_READABLE;
					Log.e(LOG_TAG, DATA_NOT_READABLE);
				}
			}
			return resultStrs;
		}
	}*/
		//endregion
}